//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2021/02/27
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QtConcurrent>

#include <core.tpp>
#include <SApplication.hpp>

#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent},
	m_death{":/death.gif"}
{
	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	movieLabel->setMovie(&m_death);
	m_death.setSpeed(500);
	reportWidget->addColumns(QList{Model, Type, Animation, Ticks, Proxy, Bone, Show, Hide, Total, FPS});
	reportWidget->hideColumn(LogWidget::Time);
	reportWidget->hideColumn(LogWidget::Log);
	connectActions();
	connectWidgets();
	connect(&m_watcher, &QFutureWatcherBase::progressRangeChanged, progressBar, &ProgressBarButton::setRange);
	connect(&m_watcher, &QFutureWatcherBase::progressValueChanged, progressBar, &ProgressBarButton::setValue);
	connect(&m_watcher, &QFutureWatcherBase::resultReadyAt, this, [this](int resultIndex)
	{
		for (auto const &report: m_watcher.resultAt(resultIndex))
		{
			QVariantList description;

			description << report.model
						<< report.type
						<< report.animation
						<< report.ticks
						<< report.proxy
						<< report.bone
						<< report.show
						<< report.hide
						<< report.total
						<< report.fps;
			if (report.type != "SOUND" && report.type != "SURFACE")
				reportWidget->addLog(LogWidget::Warning, {}, description);
			else
				reportWidget->addLog(LogWidget::Information, {}, description);
		}
	});
	connect(&m_watcher, &QFutureWatcherBase::finished, this, [this]
	{
		reportWidget->resizeColumnsToContents();
		stackedWidget->setCurrentIndex(1);
		m_death.stop();
		progressBar->resetProgressBar();
	});
	m_settings.restoreState(this);
	inputPathLineEdit->setText(m_settings.value(InputPath).toString());
	alaFilterLineEdit->setText(m_settings.value(AlaFilters, "*die*.ala").toString());
	m_settings.restoreValues(ExcludedProxies, listWidget, {});
	sApp->stopSplashScreen(this);
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(InputPath, inputPathLineEdit->text());
	m_settings.setValue(AlaFilters, alaFilterLineEdit->text());
	m_settings.saveValues(ExcludedProxies, listWidget);
}

void MainWindow::connectActions()
{
	listWidget->setOpenOption(PathListWidget::File, "Proxy (*.alo)");
	addToolButton->setDefaultAction(listWidget->actions()[PathListWidget::Add]);
	removeToolButton->setDefaultAction(listWidget->actions()[PathListWidget::Remove]);
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
}

void MainWindow::connectWidgets()
{
	connect(inputPushButton, &QPushButton::clicked, this, [this]
	{
		inputPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(inputPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		progressBar->setDisabled(text.isEmpty());
	});
	connect(progressBar, &ProgressBarButton::clicked, this, [this]
	{
		m_files = File::getFiles(inputPathLineEdit->text(), {"*.alo"});
		if (m_files.isEmpty())
			return ;
		m_death.start();
		progressBar->setFormat("%v / %m");
		m_excludedProxies.clear();
		for (int index = 0; index < listWidget->count(); ++index)
			m_excludedProxies << QFileInfo{listWidget->item(index)->text()}.completeBaseName();
		stackedWidget->setCurrentIndex(0);
		reportWidget->clear();
		m_watcher.setFuture(QtConcurrent::mapped(m_files, [this](QFileInfo const &file)
		{
			return getReports(file);
		}));
	});
}

MainWindow::Result MainWindow::getReports(QFileInfo const &file) const
{
	Result       result;
	Alamo::Model model{file};
	QStringList  alaFilters{alaFilterLineEdit->text().split(',')};

	for (auto &filer: alaFilters)
		filer.prepend(model.name());
	for (auto const &alaFile: file.dir().entryInfoList(alaFilters))
	{
		Alamo::Animation animation{alaFile, model};

		for (auto const &proxy: model.proxies())
			if (!m_excludedProxies.contains(QFileInfo{proxy.name}.completeBaseName(), Qt::CaseInsensitive))
				getVisibility(model, proxy, animation, 0, result);
		if (!animation.error().isNull())
		{
			Report report;

			report.model = model.name();
			report.type = animation.error();
			report.animation = animation.name();
			result << report;
		}
	}
	if (!model.error().isNull())
	{
		Report report;

		report.model = model.name();
		report.type = model.error();
		result << report;
	}
	return result;
}

void MainWindow::getVisibility(Alamo::Model const &model, Alamo::Model::Proxy const &proxy, Alamo::Animation const &animation, size_t frame, Result &result) const
{
	if (frame < animation.frameCount())
	{
		Report report;
		auto   show{animation.getVisibleFrame(proxy.bone, frame, animation.frameCount())};
		auto   hide{animation.getInvisibleFrame(proxy.bone, show, animation.frameCount())};

		if (show == -1 || hide == -1)
			return ;
		report.model = model.name();
		report.type = "SOUND";
		report.animation = animation.name();
		report.ticks = show * 100;
		report.proxy = proxy.name;
		report.bone = model.bones()[proxy.bone].name;
		report.show = show;
		report.hide = hide;
		report.total = animation.frameCount();
		report.fps = animation.fps();
		result << report;
		getVisibility(model, proxy, animation, hide, result);
	}
}

//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2021/02/27
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QFutureWatcher>
#include <QMovie>

#include <Animations.hpp>
#include <Models.hpp>
#include <Settings.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum ReportColumn
	{
		Model,
		Type,
		Animation,
		Ticks,
		Proxy,
		Bone,
		Show,
		Hide,
		Total,
		FPS
	};
	Q_ENUM(ReportColumn)
	enum SettingsKey
	{
		InputPath,
		AlaFilters,
		ExcludedProxies
	};
	Q_ENUM(SettingsKey)

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	struct Report
	{
		QString model;
		QString type;
		QString animation;
		quint64 ticks{0};
		QString proxy;
		QString bone;
		quint64 show{0};
		quint64 hide{0};
		quint64 total{0};
		quint64 fps{0};
	};

	using Result = QList<Report>;

	void connectActions();
	void connectWidgets();

	Result getReports(QFileInfo const &file) const;
	void   getVisibility(Alamo::Model const &model, Alamo::Model::Proxy const &proxy, Alamo::Animation const &animation, size_t frame, Result &result) const;

	Settings               m_settings;
	QMovie                 m_death;
	QFutureWatcher<Result> m_watcher;
	QFileInfoList          m_files;
	QStringList            m_excludedProxies;
};

#endif // MAINWINDOW_HPP
